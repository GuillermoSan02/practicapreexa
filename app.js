const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");

const ruta = require("./router/index")
const path = require("path")


const app = express();
app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public/'));

app.use(bodyparser.urlencoded({extended:true}))

//cambiar extensiones ejs a html
app.engine("html",require("ejs").renderFile)

app.use(ruta)


//Página de error va al final del get/post
app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html')
})

const puerto = 500;
app.listen(puerto,()=>{
    console.log("SE INICIO EL PUERTO")
});
