const bodyParser = require("body-parser")
const express = require("express")
const router = express.Router()

router.get("/",(req,res)=>{
    const params ={
        boleto: req.body.boleto,
        destino: req.body.destino,
        nombre: req.body.nombre,
        anios: req.body.anios,
        viaje: req.body.viaje,
        precio: req.body.precio
    }
    res.render('index.html', params);
})

router.post('/', (req,res)=>{
    const params ={
        boleto: req.body.boleto,
        destino: req.body.destino,
        nombre: req.body.nombre,
        anios: req.body.anios,
        viaje: req.body.viaje,
        precio: req.body.precio
    }
    res.render("index.html", params);
})

module.exports=router;